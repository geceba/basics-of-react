function TextField({label, name, onChange}) {
    return (
        <>
            <label>{label}</label>
            <input type="text" name={name} onChange={onChange}/>
        </>
    );
}

export default TextField;