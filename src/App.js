import {useState} from 'react';
import './App.css';
import Children from './Children';
import Button from './Button';
import TextField from './TextField';

function App() {
  const [count, setCount] = useState(0); // recuerda, [valor, funcionParaCambiarElValor]
  const [form, setForm] = useState({
    name: 'fdsf',
    lastName: 'fdsfs'
  }); // Como modificar un objeto con el state

  const handleAction = (newNumber) => {
    // como modificar valores del state
    setCount(count + newNumber) // ejemplo clasico del contador
  }
  // leer los valores de un formulario sin libs
  const handleForm = (e) => {
    const { name, value } = e.target; // recibimmos el evento, aqui aplicamos destructuracion de objetos, sacamos el name del input y el value
    setForm(form => ({
      ...form, // hace una copia del state de form
      [name]: value // reasignamos el valor con respecto al name del input
    }));
  }
  const handleSubmit = (e) => {
    e.preventDefault();
    // este handleSubmit no hace nada
    console.log('Send');
  }

  return (
    <div className="App">
      {/* Pasar valors a componentes hijos, esto se le conoce como props, puedes pasar lo que necesites */}
      <Button name="My first button" action={() => handleAction(5)} />
      {/* Ejemplo de un formulario */}
      <form onSubmit={handleSubmit}>
        <TextField name="name" label="Name" onChange={handleForm}/>
        <TextField name="lastName" label="Last Name" onChange={handleForm}/>
        <button type="submit">Send</button>
      </form>
      {/* Esto es un wrap, son muy utiles para realizar algun tipo de validacion antes que se haga el render o dar estilos a un wrap */}
      <Children>
        <div>
          <h1>{form.name}</h1> {/* Mostrar los valores del state en la webapp */}
          <h1>{form.lastName}</h1>
        </div>
      </Children>
    </div>
  );
}

export default App;
