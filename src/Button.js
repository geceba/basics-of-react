function Button({name, action, classes}) {
    return (
        <div>
            {/* recuerda tu puedes hacer destructuracion de objetos del prop y tu codigo queda mas legible */}
            <button onClick={action}>{name}</button>
        </div>
    );
}

export default Button;